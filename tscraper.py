# Autores: Fernando Carranza y Cristina Pradier

######################
# Libraries
######################

import re
import tweepy
import nltk
import sys
import os.path
from nltk.tokenize import word_tokenize
from collections import Counter
from nltk.probability import FreqDist
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream

#######################

if not os.path.exists('tweets-data'):
    os.makedirs('tweets-data')

#####################
# Access passwords
from passwords import consumer_token, consumer_secret, access_token, access_token_secret
auth = tweepy.OAuthHandler(consumer_token, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth,wait_on_rate_limit=True,wait_on_rate_limit_notify=True)

##########################
 
# This function does the query in Twitter and saves it in a csv file
# filepath is the location of the file in which the results will be saved
# The cursor specifies the chain to be search, language, location, etc.
# Example of cursor is the following:
# cursor = tweepy.Cursor(api.search, q=searchString, lang="es", tweet_mode="extended")

def do_the_q_csv(filepath, cursor):
    if not os.path.isfile(filepath):
        with open(filepath, 'a', encoding='utf-8') as f:
            linea1='Tweet'+'\n'
            f.write(linea1)
        do_the_q_csv(filepath, cursor)
    else:
        with open(filepath, 'a', encoding='utf-8') as f:
            for tweet in cursor.items(100):
                texto = tweet._json["full_text"].replace('\n',' ')
                texto = texto.lower()
                texto = ' '.join(re.sub("(@[a-z0-9_]+)", "", texto).split()) # deletes user names
                texto = ' '.join(re.sub("(\w+:\/\/\S+)", "", texto).split()) # deletes web links
                texto = ' '.join(re.sub("((#[A-Za-z0-9]+ ?)+$)", "", texto).split()) # deletes hashtags
                texto = ' '.join(re.sub("[\.\,\!\?\:\;\-\=¿¡\|\(\)#\[\]\"]", "", texto).split()) # deletes punctuation
                texto = ' '.join(re.sub("((ja|Ja)+)", "", texto).split()) # deletes laughters
                texto = ' '.join(re.sub("["
                u"\U0001F600-\U0001F64F"  # emoticons
                u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                u"\U0001F680-\U0001F6FF"  # transport & map symbols
                u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                u"\U00002500-\U00002BEF"  # chinese char
                u"\U00002702-\U000027B0"
                u"\U00002702-\U000027B0"
                u"\U000024C2-\U0001F251"
                u"\U0001f926-\U0001f937"
                u"\U00010000-\U0010ffff"
                u"\u2640-\u2642"
                u"\u2600-\u2B55"
                u"\u200d"
                u"\u23cf"
                u"\u23e9"
                u"\u231a"
                u"\ufe0f"  # dingbats
                u"\u3030"
                           "]+", "", texto, flags=re.UNICODE).split()) # deletes emojis
                texto = ' '.join(re.sub("((%|@|&|\*)+)", "", texto).split()) # deletes symbols
                texto = ' '.join(re.sub("([0-9]+/[0-9]+(/[0-9]+)?)", "", texto).split()) # deletes dates
                linea2=texto+'\n'
                f.write(linea2)

###############################

# This functions does the query and saves it in a txt file.
# filepath is the location of the file in which the results are to be saved
# The cursor specifies the chain to be search, language, location, etc.
# Example of cursor is the following:
# cursor = tweepy.Cursor(api.search, q=searchString, lang="es", tweet_mode="extended")
 
def do_the_q_txt(filepath, cursor):
    maxCount = 10000
    count = 0
    for tweet in cursor.items():    
    #    print(tweet.retweeted_status.full_text if tweet.full_text.startswith("RT @") else tweet.full_text, file=open("tweets-data/kuka.txt", "a", encoding='utf8')) # This line is to get complete retweets, 
        print(tweet.full_text, file=open(filepath, "a", encoding='utf8'))
    #    print("Coordinates: ", tweet.coordinates, file=open(filepath, "a", encoding='utf8'))
#    print("Place: ", tweet.place, file=open(filepath, "a", encoding='utf8'))
#    print()
        print("================================", file=open(filepath, "a", encoding='utf8'))
#    print("Location: ", tweet.user.location, file=open(filepath, "a", encoding='utf8'))
#    print("Geo Enabled? ", tweet.user.geo_enabled, file=open(filepath, "a", encoding='utf8'))
        count = count + 1
        if count == maxCount:
            break;

###############################

# This function is to count token and types of all the tweets. 
# The argument is the column of the csv which contains the tweets
def countwords(doctocount):
    total_string = ""
    for line in doctocount:
        for word in line:
            total_string = total_string + str(word) + " "
    #print(total_string)
    total_string = nltk.word_tokenize(total_string)
    dimensions = Counter(total_string)
    #print(dimensions)
    return dimensions
    totaldimensions = FreqDist(total_string)
    #print(totaldimensions)
    return totaldimensions


