# Autores= Fernando Carranza y Cristina Pradier

import pandas as pd

data = pd.read_csv("tweets-data/trolo.csv", sep=";", header=None, dtype={"0": str, "1": str, "2": str})

# Armo la función para especificar el documento, el index de las dos columnas y 
# el valor (POS, NEG o NEU) a comparar, para no reescribir todo el código por cada valor
# Llamarla por ejemplo con evaluateprecandrecall(df, 2, 3, 'POS')
def evaluateprecandrecall(df, tocompare1, tocompare2, value):
    cfp = 0 # Falsos positivos
    cfn = 0 # Falsos negativos
    cvp = 0 # Verdaderos positivos
    cvn = 0 # Verdaderos negativos
    for index, row in df.iterrows():
        if row[tocompare1] == value and row[tocompare2] == value:
            cvp = cvp + 1
        elif row[tocompare1] != value and row[tocompare2] == value:
            cfn = cfn + 1
        elif row[tocompare1] == value and row[tocompare2] != value:
            cfp = cfp + 1
        elif row[tocompare1] != value and row[tocompare2] != value:
            cvn = cvn + 1
    print(cvp)
    print(cfp)
    print(cvn)
    print(cfn)
    precision = cvp / (cvp + cfp)
    print('precision={}'.format(precision))
    cobertura = cvp / (cvp + cfn)
    print('cobertura={}'.format(cobertura))
    return precision, cobertura

evaluateprecandrecall(data, 1, 2, 'POS')
evaluateprecandrecall(data, 1, 2, 'NEU')
evaluateprecandrecall(data, 1, 2, 'NEG')

#Resultados para bostero.csv al 1/7/21, con 200 tweets
#POS: precision=0.39130434782608  cobertura=0.2647058823529412
#NEU: precision=0.5223880597014925  cobertura=0.3804347826086957
#NEG: precision=0.4909090909090909  cobertura=0.7297297297297297

#Resultados para bostero_rel.csv al 1/7/21, con 149 tweets, todos relevantes
#POS: precision=0.6428571428571429  cobertura=0.2647058823529412
#NEU: precision=0.3469387755102041  cobertura=0.4146341463414634
#NEG: precision=0.627906976744186  cobertura=0.7297297297297297

#Resultados para bostero_rel.csv al 5/7/21, con 221 tweets, todos relevantes
#POS: precision=0.642857142857142  cobertura=0.17307692307692307
#NEU: precision=0.3469387755102041  cobertura=0.3148148148148148
#NEG: precision=0.6395348837209303  cobertura=0.4782608695652174

#Resultados para trolo con 108 tweets
#POS: precision=0.6  cobertura=0.21428571428571427
#NEU: precision=0.6  cobertura=0.5853658536585366
#NEG: precision=0.6349206349206349  cobertura=0.7547169811320755



#GRAFICOS
import matplotlib.pyplot as plt
import numpy as np

labels = ['precision', 'recall']
POS = [0.642857142857142, 0.17307692307692307]
NEU = [0.3469387755102041, 0.3148148148148148]
NEG = [0.6395348837209303, 0.4782608695652174]
#ultimos valores para bostero
x = np.arange(len(labels))
width = 0.3

fig, ax = plt.subplots()
rects1 = ax.barh(x + width, POS, width, label='POS')
rects2 = ax.barh(x, NEU, width, label='NEU')
rects3 = ax.barh(x - width, NEG, width, label='NEG')
ax.set_xlim(right=1.0)
ax.set_yticks(x)
ax.set_yticklabels(labels)
ax.legend()

ax.bar_label(rects1, padding=3)
ax.bar_label(rects2, padding=3)
ax.bar_label(rects3, padding=3)

fig.tight_layout()
plt.show()