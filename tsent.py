# Autores= Fernando Carranza y Cristina Pradier

#####################
# Libraries
#####################
import re
from pysentimiento import SentimentAnalyzer
# If pysentimiento is used, cite paper at https://arxiv.org/abs/2106.09462

# Sentiment with Pysentimiento

# Language specification is not working

# This function applies Pysentimiento to a file and save the results in another file
# To do: It would be useful to make this function to add the sentiment result in the same file
def sent_with_pysentimiento(doc_to_sent, doc_to_save):
    analyzer = SentimentAnalyzer(lang="es") # specify language
    with open(doc_to_sent, 'r') as f: #source file
        tweets = f.readlines()
        with open(doc_to_save, 'a') as g: #select where to store
            g.write('Tweet ; Pysentimiento\n')
            for tweet in tweets:
                sent = analyzer.predict(tweet).output #applies pysentimiento sentiment analyzer to the tweets
                line=re.sub("\n", "", tweet)+' ; '+sent+'\n'
                g.write(line)
