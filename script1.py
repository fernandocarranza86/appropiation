# Autores: Fernando Carranza y Cristina Pradier
#
# This script is for the query of the data.

import tweepy
from tscraper import *
from tsent import *
from tpreparation import *

###############################
# Query
################################

# Load query parameters

searchString = "bolita OR bolitas -filter:retweets"
#geo="-34.628305,-58.446707,500km"
cursor = tweepy.Cursor(api.search, q=searchString, lang="es", tweet_mode="extended")
#, geocode=geo

# Run the query function (with some filtering of the data)
do_the_q_csv("tweets-data/bolita5.csv", cursor)
#do_the_q_txt("tweets-data/bolita4.txt", cursor)

###############################
# Data cleaning
################################

# See tpreparation.py
#drop_irrelevant("tweets-data/bostero.csv", "tweets-data/bostero_rel.csv", 'bostero')


#removestopwords(df[1])

###############################
# Sentiment
################################

# See tpreparation.py 
# It doesn't work due to lang parameter.
#sent_with_pysentimiento(bolita5.csv, bolita5sent.csv)


