## Descripción

This repository aims to provide and test some tools in order to study peyorative and non peyorative uses of slurs is social networks, specially twitter. For the moment it contains the following:

		1. tscraper.py:  
		2. tweets-data

The main code works on Python 3.

## tscraper.py

This module contains some functions in order to search tweets and saved them into csv or txt files.
In order to make it work, first complete your own passwords for the twitter API in the file passwords.py
The main function is do_the_q_csv. Besides the query, this functions also cleans the text. It turns all the text into lower case and deletes punctuation marks, hashtags, user names and links. 

## Authors

Fernando Carranza and Cristina Pradier

