# Autores: Fernando Carranza y Cristina Pradier
#
# This script takes the csv with tweets in the column number 1, deletes stopwords,
# lemmatizes it and count tokens and types.
# This step is made in order to reduce dimensionality in order to make a clustering 

import spacy
import re
from spacy.lang.es import Spanish
import tweepy
import pandas as pd
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from collections import Counter
from nltk.probability import FreqDist

####################
# Load the csv
df = pd.read_csv("tweets-data/bostero_rel.csv", sep=";", header=None, dtype={"0": str, "1": str, "2": str})

####################
# Tokenize
df[0] = df[0].apply(word_tokenize)
print(df[0])

#########################
# Filter stopwords
df2 = pd.read_csv("stopwords-spanish.csv", sep=";", header=None, dtype={"0": str})
#print(df2)
def removestopwords(list_of_words):
#    list_of_words = tokenizationnltk(list_of_words)
    contents = list(df2[0])
    #print(contents)
    non_important_words = []
    list_of_lines = []
    for line in list_of_words:
        important_words = []
        for word in line:
            if str(word) not in contents:
                important_words.append(word)
            else:
                non_important_words.append(word)
        list_of_lines.append(important_words)
#    print(non_important_words)
#    print(list_of_lines)
#    return list_of_lines
    df[0] = list_of_lines 
removestopwords(df[0])
print(df[0])

###############################
# lemmatize
nlp = spacy.load('es')
def lem_with_spacy(list_of_words):
    tokenized_tweets = []
    for line in list_of_words:
        tokenized_tweet = []
        for token in nlp(str(line)):
            tokenized_tweet.append(token.lemma_)
        tokenized_tweets.append(tokenized_tweet)
#    print(non_important_words)
#    print(list_of_lines)
    df[0] = tokenized_tweets 
lem_with_spacy(list(df[0]))
print(df[0])


# count the types and tokens
doctocount = df[0]
total_string = ""
for line in doctocount:
    for word in line:
        total_string = total_string + str(word) + " "
print(total_string)
total_string = ' '.join(re.sub("((ja|Ja)+)", "", total_string).split()) # deletes laughters
total_string = ' '.join(re.sub("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
        u"\U00002500-\U00002BEF"  # chinese char
        u"\U00002702-\U000027B0"
        u"\U00002702-\U000027B0"
        u"\U000024C2-\U0001F251"
        u"\U0001f926-\U0001f937"
        u"\U00010000-\U0010ffff"
        u"\u2640-\u2642"
        u"\u2600-\u2B55"
        u"\u200d"
        u"\u23cf"
        u"\u23e9"
        u"\u231a"
        u"\ufe0f"  # dingbats
        u"\u3030"
                           "]+", "", total_string, flags=re.UNICODE).split()) # deletes emojis
total_string = ' '.join(re.sub("((%|@|\*|'|,|\.|:|\[|\])+)", "", total_string).split()) # deletes symbols
total_string = ' '.join(re.sub("([0-9]+/[0-9]+(/[0-9]+)?)", "", total_string).split()) # deletes dates
total_string = ' '.join(re.sub("(bostero(s?))", "", total_string).split()) # deletes 
total_string = nltk.word_tokenize(total_string)

dimensions = Counter(total_string)
print(dimensions)
totaldimensions = FreqDist(total_string)
print(totaldimensions) #print(dimensions)



