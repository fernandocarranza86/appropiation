# Autores: Fernando Carranza y Cristina Pradier

#####################
# Libraries
#####################

import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import *
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import spacy
from spacy.lang.es import Spanish
# Install first the Spanish model with python3 -m spacy download es or python -m spacy download 
# depending on the availability of both versions of python or not.

#########################
# Load the csv

# Select here the csv
#df = pd.read_csv("tweets-data/bostero_rel.csv", sep=";", header=None, dtype={"0": str, "1": str, "2": str})
#df = pd.read_csv("tweets-data/bostero2.csv", sep=";", header=None, dtype={"0": str})
#print(df)

#########################
# Tweets filter

# this function drops all the irrelevant lines in a given file
# fromfile is the path to the source file
# tofile is the path to the new file where the relevant lines will be saved
# relword is the relevant term

def drop_irrelevant(fromfile, tofile, relword):
    with open(fromfile, 'r') as f:
        lines = f.readlines()
        with open(tofile, 'a') as g:
            for line in lines:
                if relword in line:
                    g.write(line)


#########################
# Tokenization of the tweets
def tokenizationnltk(doc_to_tokenize):
    doc_to_tokenize = doc_to_tokenize.apply(word_tokenize)
    df[1] = doc_to_tokenize
#    print(doc_to_tokenize)
#df[1] = df[1].apply(word_tokenize)

##############################
# Removing stopwords from tweets


df2 = pd.read_csv("stopwords-spanish.csv", sep=";", header=None, dtype={"0": str})
#print(df2)


def removestopwords(list_of_words):
#    list_of_words = tokenizationnltk(list_of_words)
    contents = list(df2[0])
    #print(contents)
    non_important_words = []
    list_of_lines = []
    for line in list_of_words:
        important_words = []
        for word in line:
            if str(word) not in contents:
                important_words.append(word)
            else:
                non_important_words.append(word)
        list_of_lines.append(important_words)
#    print(non_important_words)
#    print(list_of_lines)
#    return list_of_lines
    df[1] = list_of_lines 
# If I write list_of_words instead of df[1] in the last line it does not work the replacement
    
#removestopwords(df[1])
#print(df)

###########################################
# Lemmatizing
# Maybe it would be a good idea to add here a Lemmatizer. 
# NLTK does not have any lemmatizer. Spacy's does not use tags and it is not recommended
# StanfordCoreNLP's is recommended in some posts.

nlp = spacy.load('es')

def lem_with_spacy(list_of_words):
    tokenized_tweets = []
    for line in list_of_words:
        tokenized_tweet = []
        for token in nlp(line):
            tokenized_tweet.append(token.lemma_)
        tokenized_tweets.append(tokenized_tweet)
#    print(non_important_words)
#    print(list_of_lines)
    list_of_words = tokenized_tweets 
# If I write list_of_words instead of df[1] in the last line it does not work the replacement.

#lem_with_spacy(df[1])
#print(df[1])

#########################################

##############################

# Vectorization of the tweets


#tfidf_vectorizer = TfidfVectorizer(min_df=10)
#X_train_tfidf = tfidf_vectorizer.fit_transform(df[1])

# Maybe word2vec?

