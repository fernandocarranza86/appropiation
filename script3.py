# Autores: Fernando Carranza y Cristina Pradier
#
# This script takes only tweets from the csv file which are manually 
# tagged as negatives and count tokens and types.
# It also lemmatizes them and count tokens and types of lemmas

import spacy
import re
from spacy.lang.es import Spanish
import pandas as pd
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from collections import Counter
from nltk.probability import FreqDist

####################
# Load the csv
df = pd.read_csv("tweets-data/bostero_rel.csv", sep=";", header=None, dtype={"0": str, "1": str, "2": str})

def filterdettweets(value):
    global filtered_tweets
    filtered_tweets = []
    for index, row in df.iterrows():
        if row[2] == value:
            filtered_tweets.append(row[0])
   
    
filterdettweets('NEG')
print(filtered_tweets)

# count the types and tokens
total_string = ""
for line in filtered_tweets:
    total_string = total_string + line + " "
print(total_string)
total_string = ' '.join(re.sub("((ja|Ja)+)", "", total_string).split()) # deletes laughters
total_string = ' '.join(re.sub("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
        u"\U00002500-\U00002BEF"  # chinese char
        u"\U00002702-\U000027B0"
        u"\U00002702-\U000027B0"
        u"\U000024C2-\U0001F251"
        u"\U0001f926-\U0001f937"
        u"\U00010000-\U0010ffff"
        u"\u2640-\u2642"
        u"\u2600-\u2B55"
        u"\u200d"
        u"\u23cf"
        u"\u23e9"
        u"\u231a"
        u"\ufe0f"  # dingbats
        u"\u3030"
                           "]+", "", total_string, flags=re.UNICODE).split()) # deletes emojis
total_string = ' '.join(re.sub("((%|@|\*|'|,|\.|:|\[|\])+)", "", total_string).split()) # deletes symbols
total_string = ' '.join(re.sub("([0-9]+/[0-9]+(/[0-9]+)?)", "", total_string).split()) # deletes dates
total_string = ' '.join(re.sub("(bostero(s?))", "", total_string).split()) # deletes 
total_string = nltk.word_tokenize(total_string)

dimensions = Counter(total_string)
print(dimensions)
totaldimensions = FreqDist(total_string)
print(totaldimensions) #print(dimensions)
df2 = pd.read_csv("stopwords-spanish.csv", sep=";", header=None, dtype={"0": str})

def removestopwords(list_of_words):
    wordsstring = ' '.join([str(item) for item in list_of_words])
    print(wordsstring)
    wordsstring = word_tokenize(wordsstring)
    contents = list(df2[0])
    #print(contents)
    non_important_words = []
    global list_of_lines
    list_of_lines = []
    important_words = []    
    for word in wordsstring:
        if str(word) not in contents:
            important_words.append(word)
        else:
            non_important_words.append(word)
    list_of_lines.append(important_words)
#    print(non_important_words)
#    print(list_of_lines)
#    return list_of_lines
removestopwords(filtered_tweets)
print(list_of_lines)

# lemmatize
nlp = spacy.load('es')
def lem_with_spacy(list_of_words):
    wordsstring = ' '.join([str(item) for item in list_of_words])
    global tokenized_tweets
    tokenized_tweets = []
    tokenized_tweet = []
    for word in nlp(wordsstring):
        tokenized_tweets.append(word.lemma_)
#    print(non_important_words)
#    print(list_of_lines)
lem_with_spacy(list_of_lines)
print(tokenized_tweets)

total_string = ''
for line in tokenized_tweets:
    total_string = total_string + str(line) + " "
print(total_string)
total_string = ' '.join(re.sub("((ja|Ja)+)", "", total_string).split()) # deletes laughters
total_string = ' '.join(re.sub("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
        u"\U00002500-\U00002BEF"  # chinese char
        u"\U00002702-\U000027B0"
        u"\U00002702-\U000027B0"
        u"\U000024C2-\U0001F251"
        u"\U0001f926-\U0001f937"
        u"\U00010000-\U0010ffff"
        u"\u2640-\u2642"
        u"\u2600-\u2B55"
        u"\u200d"
        u"\u23cf"
        u"\u23e9"
        u"\u231a"
        u"\ufe0f"  # dingbats
        u"\u3030"
                           "]+", "", total_string, flags=re.UNICODE).split()) # deletes emojis
total_string = ' '.join(re.sub("((%|@|\*|'|,|\.|:|\[|\])+)", "", total_string).split()) # deletes symbols
total_string = ' '.join(re.sub("([0-9]+/[0-9]+(/[0-9]+)?)", "", total_string).split()) # deletes dates
total_string = ' '.join(re.sub("(bostero(s?))", "", total_string).split()) # deletes 
total_string = nltk.word_tokenize(total_string)

dimensions = Counter(total_string)
print(dimensions)
totaldimensions = FreqDist(total_string)
print(totaldimensions) #print(dimensions)


